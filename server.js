'use strict';

const Hapi = require('@hapi/hapi');
const jwtAuth = require('hapi-auth-jwt2');
const routes = require('./src/routes');
const db = require('./src/db/redis');

// Auth validation
const validate = async (decoded, request, h) => {
	return await db.users.validToken(decoded);
};

const init = async () => {
	const server = Hapi.Server({
		port: 3000,
		host: 'localhost',
	});

	// register auth jwt to server
	await server.register(jwtAuth);

	// define auth strategy
	server.auth.strategy('jwt', 'jwt', {
		key: process.env.JWT || 'DontShareThis!!',
		validate,
		validateOptions: {
			algorithms: ['HS256'],
		},
	});

	server.auth.default('jwt');

	server.route(routes);

	await server.start();
	console.log('Server running on %s', server.info.uri);
	return server;
};

process.on('error', err => {
	console.log(err);
	process.exit();
});

process.on('unhandledRejection', err => {
	console.log(err);
	process.exit();
});

exports.init = init;
