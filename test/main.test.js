'use strict';

const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it } = (exports.lab = Lab.script());
const { init } = require('../server');

describe('Token auth test case', () => {
	let server;

	beforeEach(async () => {
		server = await init();
	});

	afterEach(async () => {
		await server.stop();
	});

	it('Can access to root of routes without authorization token', async () => {
		const res = await server.inject({
			method: 'GET',
			url: '/',
		});

		expect(res.statusCode).to.equal(200);
	});

	it('Unauthorized access responds with 401', async () => {
		const res = await server.inject({
			method: 'GET',
			url: '/chats',
		});

		expect(res.statusCode).to.equal(401);
	});

	it("Can access to 'chats endpoint with valid token", async () => {
		const res = await server.inject({
			method: 'GET',
			url: '/chats',
			headers: {
				Authorization:
					'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiU29sb24gUG93bG93c2tpIiwiZW1haWwiOiJPbWVyLk1hbm5AeWFob28uY29tIiwiaWF0IjoxNTc4OTkxNDM4fQ.vQ2dtdlKH1Yo2O8re80qusGCxPx0Rv4Xr39ikpwAyng',
			},
		});

		expect(res.statusCode).to.equal(200);
	});

	it('Can request token with valid email and password', async () => {
		const res = await server.inject({
			method: 'POST',
			url: '/generate_token',
			payload: {
				email: 'Omer.Mann@yahoo.com',
				password: 'password',
			},
		});

		const payload = res.payload ? JSON.parse(res.payload) : null;

		expect(res.statusCode).to.equal(200);
		expect(payload.status).to.equal(200);
		expect(payload.token).to.not.equal(null);
	});
});
