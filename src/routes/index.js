const JWT = require('jsonwebtoken');
const db = require('./../db/redis');
const bcrypt = require('bcrypt');
const filter = require('lodash/filter');
const map = require('lodash/map');
const range = require('lodash/range');
const faker = require('faker');
const { getUnixTime, addDays } = require('date-fns');

const routes = [
	{
		method: 'GET',
		path: '/',
		config: { auth: false },
		handler: (req, h) => {
			return {
				status: 200,
				message: 'Welcome to Hapi API application example',
				version: 1,
			};
		},
	},
	{
		method: 'GET',
		path: '/test',
		config: { auth: false },
		handler: (req, h) => {
			return 'Hello World';
		},
	},
	{
		method: 'GET',
		path: '/chats',
		config: { auth: 'jwt' },
		handler: (req, h) => {
			return 'Chat';
		},
	},
	{
		method: 'GET',
		path: '/users',
		config: { auth: 'jwt' },
		handler: async (req, h) => {
			return await db.users.all();
		},
	},
	{
		method: 'GET',
		path: '/populate',
		config: { auth: false },
		handler: async (req, h) => {
			const saltRounds = 10;
			const people = await Promise.all(
				map(range(1, 10), () => {
					return {
						name: faker.name.findName(),
						avatar: faker.image.avatar(),
						email: faker.internet.email(),
						password: bcrypt.hashSync('password', saltRounds),
					};
				}),
			);

			return db.users.populate(people);
		},
	},
	{
		method: 'POST',
		path: '/generate_token',
		config: { auth: false },
		handler: async (req, h) => {
			const users = await db.users.all();
			const data = req.payload;
			const user = filter(users, e => e.email === data.email).pop();

			if (user && bcrypt.compareSync(data.password, user.password)) {
				const token = await JWT.sign(
					{ name: user.name, email: user.email },
					'DontShareThis!!',
				);

				return {
					status: 200,
					token,
					tokenExpired: getUnixTime(addDays(new Date(), 7)),
				};
			} else {
				return {
					status: 403,
					message: 'Invalid credentials',
				};
			}
		},
	},
];

module.exports = routes;
