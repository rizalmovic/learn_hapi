const redisClient = require('./client');
const { promisify } = require('util');

const getAsync = promisify(redisClient.get).bind(redisClient);

// Users DB example
const users = {
	all: () => {
		return getAsync('users').then(result => JSON.parse(result));
	},
	getByEmail: email => {
		const _users = getAsync('users').then(result => JSON.parse(result));
		const _user = _users.filter(e => e.email === email);

		if (_user.length) {
			return '';
		}
	},
	populate: async data => {
		return redisClient.set('users', JSON.stringify(data));
	},
	validToken: async credentials => {
		const _users = await users.all();

		if (_users.filter(e => e.email === credentials.email).length) {
			return { isValid: true };
		} else {
			return { isValid: false };
		}
	},
};

module.exports = users;
