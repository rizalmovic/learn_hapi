const client = require('./client');
const { promisify } = require('util');
const map = require('lodash/map');
const range = require('lodash/range');

const getAsync = promisify(client.get).bind(client);

// Users DB example
const users = require('./users');

module.exports = {
	users,
};
